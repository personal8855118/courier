import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const storage = FlutterSecureStorage();

Future<String> authenticateUser(String username, String password) async {
  final url = Uri.parse('https://app.eps-int.com/login');
  final authenticateReponse = await http.post(url,
      body: {'username': username, 'password': password},
      headers: {'Content-Type': 'application/x-www-form-urlencoded'});

  String cookies = authenticateReponse.headers['set-cookie']!;

  // Guardar la cookie de manera segura en el almacenamiento
  await storage.write(key: 'cookie', value: cookies);

  if (authenticateReponse.statusCode == 302) {
    return authenticateReponse.body;
  } else {
    throw Exception(authenticateReponse.statusCode);
  }
}

logoutUser() async {
  await storage.delete(key: 'cookie');
}