import 'package:http/http.dart' as http;
import 'package:html/parser.dart' show parse;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const storage = FlutterSecureStorage();

Future<List<String>> packageList() async {
  final storedCookie = await storage.read(key: 'cookie');
  final url = Uri.parse('https://app.eps-int.com/TrackingPaquetes');
  final response = await http.get(url, headers: {'Cookie': storedCookie ?? ''});

  return parse(response.body)
      .querySelectorAll('.grid-item')
      .map((element) =>
          '${element.attributes['data-title']} - ${element.attributes['data-date']} - ${element.attributes['data-groups']?.split(' ').where((state) => state.contains('status')).map((state) => {
                'status1': 'Origen',
                'status2': 'Aéreo',
                'status3': 'Aduana',
                'status4': 'Distribución',
                'status5': 'Disponible',
                'status6': 'Tránsito'
              }[state]).join(', ')}')
      .toList();
}