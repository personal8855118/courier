import 'package:courier/data/services/user_auth_service.dart';
import 'package:courier/presentation/screens/home_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                enableSuggestions: true,
                controller: username,
                decoration: const InputDecoration(
                    labelText: 'EPS Code', border: OutlineInputBorder()),
              ),
              const SizedBox(
                height: 16.0,
              ),
              TextFormField(
                obscureText: true,
                enableSuggestions: false,
                controller: password,
                decoration: const InputDecoration(
                    labelText: 'Password', border: OutlineInputBorder()),
              ),
              const SizedBox(
                height: 32.0,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  onPressed: valitation,
                  child: const Text('Continue'),
                ),
              ),
            ]),
      ),
    );
  }

  Future<void> valitation() async {
    try {
      String response = await authenticateUser(username.text, password.text);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const HomeScreen()));
      setState(() {
        messageAuthenticateUser('Success');
      });
    } catch (e) {
      setState(() {
        messageAuthenticateUser('Failure: $e');
      });
    }
  }

  void messageAuthenticateUser(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
