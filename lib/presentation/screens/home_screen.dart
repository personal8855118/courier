import 'package:courier/data/services/user_auth_service.dart';
import 'package:courier/presentation/screens/login_screen.dart';
import 'package:flutter/material.dart';

import '../../data/services/package_storage_service.dart';
import '../../utils/package_states.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> packages = [];
  final List<String> _chipFilter = [
    'Disponible',
    'Tránsito',
    'Distribución',
    'Aduana',
    'Aéreo',
    'Origen'
  ];

  String _selectedFilter = 'Todos';

  Future<void> getPackages() async {
    final packagesList = await packageList();
    setState(() {
      packages = packagesList;
    });
  }

  /*final List<Color> _iconColors = [
    Colors.green, // Disponible
    Colors.blue, // Tránsito
    Colors.orange, // Distribución
    Colors.red, // Aduana
    Colors.purple, // Aéreo
    Colors.yellow, // Origen
  ];*/

  Icon _getIconForState(String state, int index) {
    final icons = {
      'Origen': Icons.flight_takeoff,
      'Aéreo': Icons.airplanemode_active,
      'Aduana': Icons.policy,
      'Distribución': Icons.local_shipping,
      'Disponible': Icons.inventory_2,
      'Tránsito': Icons.compare_arrows,
    };
    final iconData = icons[state] ?? Icons.help_outline;
    return Icon(
      iconData,
      //color: _iconColors[index % _iconColors.length],
    );
  }

  List<String> _getFilteredPackages() {
    if (_selectedFilter == 'Todos') {
      return packages;
    } else {
      return packages
          .where((package) => package.contains(_selectedFilter))
          .toList();
    }
  }

  @override
  void initState() {
    super.initState();
    getPackages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Courier'),
          actions: [
            IconButton(onPressed: getPackages, icon: const Icon(Icons.refresh)),
            PopupMenuButton(itemBuilder: (context) {
              return [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Logout"),
                ),
              ];
            }, onSelected: (value) {
              if (value == 0) {
                logoutUser();
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LoginScreen()));
              }
            }),
          ],
        ),
        body: Column(
          children: [
            SizedBox(
              height: 50,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: _chipFilter.length,
                itemBuilder: (BuildContext context, int index) {
                  final label = _chipFilter[index];
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: FilterChip(
                      label: Text(label),
                      selected: _selectedFilter == label,
                      onSelected: (isSelected) {
                        setState(() {
                          _selectedFilter = isSelected ? label : 'Todos';
                        });
                      },
                    ),
                  );
                },
              ),
            ),
            Expanded(
              child: ListView.separated(
                itemCount: _getFilteredPackages().length,
                separatorBuilder: (context, index) => const SizedBox(height: 8),
                itemBuilder: (context, index) {
                  final packageInfo =
                      _getFilteredPackages()[index].split(' - ');
                  final title = packageInfo[0];
                  final date = packageInfo[1];
                  final state = packageInfo[2];
                  final icon = _getIconForState(state, index);
                  return Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8),
                      child: Card(
                        child: Column(
                          children: [
                            ListTile(
                              leading: CircleAvatar(
                                child: icon,
                              ),
                              title: Text(title),
                              subtitle: Text(state),
                              trailing: Text(date),
                            ),
                            SliderTheme(
                              data: SliderTheme.of(context).copyWith(
                                disabledActiveTrackColor:
                                    Theme.of(context).colorScheme.primary,
                                disabledInactiveTrackColor: Theme.of(context)
                                    .colorScheme
                                    .primaryContainer,
                                disabledThumbColor:
                                    Theme.of(context).colorScheme.primary,
                              ),
                              child: Slider(
                                value: convertStateToNumber(packageInfo[2]),
                                min: 0,
                                max: 5,
                                divisions: 5,
                                label: state,
                                onChanged: null,
                              ),
                            )
                          ],
                        ),
                      ));
                },
              ),
            )
          ],
        ));
  }
}
