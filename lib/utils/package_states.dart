  double convertStateToNumber(String state) {
    switch (state) {
      case 'Origen':
        return 0;
      case 'Aéreo':
        return 1;
      case 'Aduana':
        return 2;
      case 'Distribución':
        return 3;
      case 'Disponible':
        return 5;
      case 'Tránsito':
        return 4;
      default:
        return 0;
    }
  }