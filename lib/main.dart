import 'package:courier/presentation/screens/home_screen.dart';
import 'package:courier/presentation/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  
  const storage = FlutterSecureStorage();
  final cookie = await storage.read(key: 'cookie');

  runApp(MaterialApp(
    theme: ThemeData(useMaterial3: true),
    debugShowCheckedModeBanner: false,
    home: cookie != null ? const HomeScreen() : const LoginScreen(),
  ));
}
